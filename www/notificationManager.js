/*
 * Licensed to Mobile-Spot *
 */

var exec = require('cordova/exec');

module.exports = {
        getPendingNotifications: function(successCallback, errorCallback) {
                if (typeof successCallback !== 'function') {
                        console.error('notificationManager.getPendingNotifications success callback must be a function, got: '+typeof successCallback);
                        return;
                }
                if (typeof errorCallback !== 'function') {
                        console.error('notificationManager.getPendingNotifications error callback must be a function, got: '+typeof errorCallback);
                        return;
                }

                exec(successCallback, errorCallback, 'NotificationManager', 'getPendingNotifications', []);
        }
};
