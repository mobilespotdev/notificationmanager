package com.mobilespot;

public interface JavascriptSender{
    void sendJavascript(final String javascript);
}
