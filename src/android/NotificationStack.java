package com.mobilespot;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Aurelien Hiron on 12/04/2019.
 */
class NotificationStack {
    private static final String TABLE_NAME = "Notifications";
    private static final String SQL_CREATE_TABLE =  "CREATE TABLE " + TABLE_NAME + " (id INTEGER PRIMARY KEY AUTOINCREMENT, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP, json TEXT)";
    private static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    private static final String SQL_SELECT_NOTIF = "SELECT id, json FROM " + TABLE_NAME + " ORDER BY timestamp DESC";

    private NotificationDbHelper dbHelper;

    public NotificationStack( Context context ) {
        this.dbHelper = new NotificationDbHelper( context );
    }

    private static class NotificationDbHelper extends SQLiteOpenHelper {
        // If you change the database schema, you must increment the database version.
        public static final int DATABASE_VERSION = 1;

        public static final String DATABASE_NAME = "pnotif.db";

        public NotificationDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_TABLE);
        }
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(SQL_DELETE_TABLE);
            onCreate(db);
        }
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }

        public void insert(SQLiteDatabase db, String json) {
            ContentValues values = new ContentValues();
            values.put("json", json);
            db.insert(TABLE_NAME, null, values);
        }

        public Cursor getAll(SQLiteDatabase db) {
            Cursor cursor = db.rawQuery( SQL_SELECT_NOTIF, null );
            return cursor;
        }

        public void remove(SQLiteDatabase db, String[] ids) {
            int res = db.delete(TABLE_NAME, "id IN ("+TextUtils.join(",", ids)+")", null);
            Log.d("res","res:"+res);
        }
    }

    public void pushNotification( Notification notification ) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
            dbHelper.insert( db, notification.getJson() );
        }catch(Exception e){
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public JSONArray pullJsonNotifications() {

        ArrayList<String> ids = new ArrayList<String>();
        JSONArray res = new JSONArray();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
            Cursor cursor = dbHelper.getAll(db);
            while (cursor.moveToNext()) {
                ids.add(cursor.getString(cursor.getColumnIndex("id")));
                String jsonString = cursor.getString(cursor.getColumnIndex("json") );
                try {
                    res.put(new JSONObject(jsonString));
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if ( ids.size() > 0 ) {
                dbHelper.remove(db, ids.toArray(new String[ids.size()]) );
            }
        }catch(Exception e){
            e.printStackTrace();
        } finally {
            db.close();
        }

        return res;
    }
}
