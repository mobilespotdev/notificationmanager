package com.mobilespot;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

/**
 * Created by Aurelien Hiron on 12/04/2019.
 */
public class Notification {

    static final String LOG_TAG = "Notification";
    private static final int NOTIF_UID = 4242; // Try to not use an existing notification id

    private int id;
    private String kind;
    private Context context;
    private String json;

    public Notification( Context context, String kind, String json ) {
        this.context = context;
        this.kind = kind;
        this.id = (int) System.currentTimeMillis();
        this.json = json;
    }

    public String getJson() {
        return json;
    }

    public void emitLocalNotification() {
        try {
            Intent notificationIntent = this.context.getPackageManager()
                    .getLaunchIntentForPackage(this.context.getPackageName());

            PendingIntent contentIntent = PendingIntent.getActivity(this.context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

            String packageName = this.context.getPackageName();
            int icon = this.context.getResources().getIdentifier("notif_icon", "drawable", packageName);
            if ( icon == 0 ) {
                // If no specific notification icon found, use default one : icon
                icon = this.context.getResources().getIdentifier("icon", "mipmap", packageName);
            }


            String title = null;
            String content = "";
            Locale locale = Locale.getDefault();

            try {
                JSONObject data = new JSONObject( json );

                if ( data.has("pw_msg") ) {
                    //Notification pushwoosh


                    if ( data.has("title") ) {
                        title = data.getString("title");
                    }

                } else {
                    // Notification naoAlerte
                    if ( data.has("id") ) {
                        this.id = data.getInt("id");
                    }

                    String lang = locale.getLanguage();

                    // Get notification title
                    if ( data.has("title") ) {

                        try {
                            JSONObject titleData = null;
                            titleData = data.getJSONObject("title");
                            if ( titleData.has(lang) ) {
                                title = titleData.getString(lang);
                            } else if ( titleData.has("en") ) {
                                title = titleData.getString("en");
                            } else {
                                Log.e(LOG_TAG, "Cannot retreive title (no langage)");
                            }
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }

                    // Get name as default title
                    if ( title == null && data.has("name") ) {
                        title = data.getString("name");
                    }

                    // Get message obj
                    JSONObject msgData = null;
                    if ( data.has("content") ) {
                        msgData = data.getJSONObject("content");
                    } else if ( data.has("text") ) {
                        msgData = data.getJSONObject("text");
                    }
                    // Get message according to langage
                    if ( msgData != null ) {
                        try {
                            if ( msgData.has(lang) ) {
                                content = msgData.getString(lang);
                            } else if ( msgData.has("en") ) {
                                content = msgData.getString("en");
                            } else {
                                Log.e(LOG_TAG, "Cannot retreive message (no langage)");
                            }
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                }

                android.app.Notification.Builder builder = new android.app.Notification.Builder(this.context);
                builder.setContentIntent( contentIntent )
                        .setAutoCancel( true )
                        .setSmallIcon( icon )
                        .setContentText( content );

                if ( title != null ) {
                    builder.setContentTitle( title );
                }
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    builder.setChannelId(this.kind+"_01");
                }

                android.app.Notification notification;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    notification = builder.build();
                } else {
                    notification = builder.getNotification();
                }
                android.app.NotificationManager nm = (NotificationManager) this.context.getSystemService(Context.NOTIFICATION_SERVICE);
                nm.notify(this.id, notification);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            Log.e( LOG_TAG, "Error on emitLocalNotification", e );
        }
    }
}
