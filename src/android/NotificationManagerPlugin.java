package com.mobilespot;

import android.app.NotificationChannel;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Aurelien Hiron on 18/04/2019.
 */
public class NotificationManagerPlugin extends CordovaPlugin implements JavascriptSender {

    public static final String LOG_TAG = "NotificationManagerPlugin";

    private CordovaWebView webView;
    private NotificationManager nm;


    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        this.webView = webView;

        Context ctx = this.cordova.getContext();

        nm = NotificationManager.getInstance( ctx );
        nm.setJavascriptSender( this );

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            ApplicationInfo applicationInfo = ctx.getApplicationInfo();
            int stringId = applicationInfo.labelRes;
            CharSequence name =  stringId == 0 ? applicationInfo.packageName.toString() : ctx.getString(stringId);
            android.app.NotificationManager anm = (android.app.NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);

            NotificationChannel channelPush = new NotificationChannel("push_01", name, android.app.NotificationManager.IMPORTANCE_DEFAULT);
            channelPush.enableLights(true);
            channelPush.setLightColor(Color.BLUE);
            channelPush.enableVibration(true);
            anm.createNotificationChannel(channelPush);

            NotificationChannel channelGeo = new NotificationChannel("geo_01", name, android.app.NotificationManager.IMPORTANCE_DEFAULT);
            channelGeo.enableLights(true);
            channelGeo.setLightColor(Color.BLUE);
            channelGeo.enableVibration(true);
            anm.createNotificationChannel(channelGeo);
        }

    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        Log.d(LOG_TAG, "executing action "+action);

        switch (action) {
            case "getPendingNotifications":
                // Pull and return notifications from stack
                JSONObject data = new JSONObject();
                try {
                    data.put("notifications", this.nm.getPendingNotifications());
                    String jsonString = data.toString();
                    callbackContext.success( jsonString );
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            default:
                callbackContext.error("Invalid action: " + action);
                return false;
        }

        return true;
    }

    @Override
    public void sendJavascript(final String javascript) {
        if (javascript != null && javascript.trim().length() > 0 && !javascript.equals("null")) {

            webView.getView().post(new Runnable() {
                @Override
                public void run() {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        webView.sendJavascript(javascript);
                    } else {
                        webView.loadUrl("javascript:" + javascript);
                    }
                }
            });
        }
    }

}
