package com.mobilespot;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;

/**
 * Created by Aurelien Hiron on 12/04/2019.
 */

public class NotificationManager {

    private static final String LOG_TAG = "NotificationManager";
    private static NotificationManager instance_;
    private Boolean foreground = false;
    private Context context;
    private NotificationStack stack;
    private JavascriptSender js;

    /*
     * Register lifecycle to get heading only in foreground
     *
     */
    private Application.ActivityLifecycleCallbacks activityCallbacks = new Application.ActivityLifecycleCallbacks() {
        @Override
        public void onActivityCreated(Activity activity, Bundle bundle) {}

        @Override
        public void onActivityStarted(Activity activity) {}

        @Override
        public void onActivityStopped(Activity activity) {}

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {}

        @Override
        public void onActivityResumed(Activity activity) {
            synchronized (foreground) {
                foreground = true;
            }
        }

        @Override
        public void onActivityPaused(Activity activity) {

            synchronized (foreground) {
                foreground = false;
            }
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            activity.getApplication().unregisterActivityLifecycleCallbacks(activityCallbacks);
        }
    };

    private boolean isInForground() {
        synchronized (foreground) {
            return foreground;
        }
    }

    private NotificationManager( Context context ) {
        this.context = context;
        stack = new NotificationStack( context );
        Application app = (Application) context.getApplicationContext();
        app.registerActivityLifecycleCallbacks( activityCallbacks );
    }

    public static synchronized NotificationManager getInstance( Context ctx ) {
        if ( instance_ != null ) {
            return instance_;
        }
        instance_ = new NotificationManager( ctx );
        return instance_;
    }

    public void onNotification( String kind, String json ) {
        Notification notif = new Notification( context, kind, json );
        stack.pushNotification( notif );

        if ( isInForground() ) {
            // if application in foreground

            try {
                // Send js event
                if ( js == null ) {
                    Log.e( LOG_TAG, "Cannot publish notification to js : JavascriptSender is missing" );
                    return;
                }
                this.js.sendJavascript("var e = new Event('onNotification'); document.dispatchEvent(e);");
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            // else emit notification
            notif.emitLocalNotification();
        }

    }

    public JSONArray getPendingNotifications() {
        return stack.pullJsonNotifications();
    }

    public void setJavascriptSender(JavascriptSender js) {
        this.js = js;
    }

}
