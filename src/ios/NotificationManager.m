//
//  NotificationStack.m
//  Siae2019_D
//
//  Created by Aurélien Hiron on 15/04/2019.
//

#import <Foundation/Foundation.h>
#import "NotificationManager.h"
#import <UserNotifications/UserNotifications.h>


#define DB_NAME "pnotifs.db"
#define TABLE_NAME "Notifications"
#define SQL_CREATE_TABLE "CREATE TABLE " TABLE_NAME " (id INTEGER PRIMARY KEY AUTOINCREMENT, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP, json TEXT)";
#define SQL_DELETE_TABLE "DROP TABLE IF EXISTS " TABLE_NAME;
#define SQL_SELECT_NOTIF "SELECT id, json FROM " TABLE_NAME " ORDER BY timestamp DESC"
#define SQL_INSERT_NOTIF "INSERT INTO " TABLE_NAME " (json) VALUES (?)"
#define SQL_DELETE_NOTIF "DELETE FROM " TABLE_NAME " WHERE id IN (%s)"

#define AlertThresholdInSecond 4*60*60 //4h

//#define JSINTERFACE_NAME "NotificationManager"

@implementation NotificationDbHelper

-(BOOL)open{
	NSString *docsDir;
	NSArray *dirPaths;

	// Get the documents directory
	dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	docsDir = [dirPaths objectAtIndex:0];
	
	// Build the path to the database file
	NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @DB_NAME]];
	
	NSFileManager *filemgr = [NSFileManager defaultManager];
	
	const char *dbpath = [databasePath UTF8String];
	if ([filemgr fileExistsAtPath: databasePath ] == NO) {
		if (sqlite3_open(dbpath, &db) == SQLITE_OK) {
			char *errMsg;
			const char *sql_stmt = SQL_CREATE_TABLE;
			
			if (sqlite3_exec(db, sql_stmt, NULL, NULL, &errMsg) == SQLITE_OK) {
				return YES;
			} else {
				NSLog(@"Failed to create table : sqlite3_exec failed");
			}
		} else {
			NSLog(@"Failed to create table : sqlite3_open failed");
		}
	} else if (sqlite3_open(dbpath, &db) == SQLITE_OK) {
		return YES;
	}
	return NO;
}

-(void)close{
	if ( db ) {
		sqlite3_close(db);
		db = NULL;
	}
}

-(BOOL)insert:(NSString *)json {
	BOOL ret = NO;
	sqlite3_stmt    *statement;

	const char *insert_stmt = [@SQL_INSERT_NOTIF UTF8String];
	sqlite3_prepare_v2(db, insert_stmt, -1, &statement, NULL);
	sqlite3_bind_text(statement, 1, [json UTF8String], -1, SQLITE_TRANSIENT);
	if (sqlite3_step(statement) == SQLITE_DONE) {
		ret = YES;
	} else {
		NSLog(@"Failed to insert notification");
	}
	sqlite3_finalize(statement);
	return ret;
}

-(NSMutableArray*)getAllAndIds:(NSMutableArray**)ids {
	NSMutableArray *notifs = NULL;
	sqlite3_stmt    *statement;
	NSString *selectSQL = @SQL_SELECT_NOTIF;
	const char *select_stmt = [selectSQL UTF8String];
	sqlite3_prepare_v2(db, select_stmt, -1, &statement, NULL);

	while(sqlite3_step(statement) == SQLITE_ROW)
	{
		if ( !notifs ) {
			notifs = [[NSMutableArray alloc] init];
			*ids = [[NSMutableArray alloc] init];
		}

		int intid = sqlite3_column_int(statement, 0);
		NSNumber *id = [[NSNumber alloc] initWithInt:intid];
		
		char *json = (char *) sqlite3_column_text(statement,1);
		NSString *jsonStr = [[NSString alloc] initWithUTF8String: json];
		
		// jsonStr to NSDictionary
		NSData *objectData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
		NSDictionary *obj = [NSJSONSerialization JSONObjectWithData:objectData
															 options:NSJSONReadingMutableContainers
															   error:NULL];
		[notifs addObject:obj];
		[*ids addObject:id];
	}
	sqlite3_finalize(statement);

	return notifs;
}

-(BOOL)remove:(NSArray*)ids {
	BOOL ret = NO;

	if ( !ids || ( [ids count] ==  0) ) {
		// Nothing to do
		return true;
	}
	sqlite3_stmt    *statement;
	NSString *ids_str = [ids componentsJoinedByString: @","];
	NSString *deleteSQL = [NSString stringWithFormat: @SQL_DELETE_NOTIF, [ids_str UTF8String] ];
	const char *delete_stmt = [deleteSQL UTF8String];
	sqlite3_prepare_v2(db, delete_stmt, -1, &statement, NULL);
	if (sqlite3_step(statement) == SQLITE_DONE) {
		ret = YES;
	} else {
		NSLog(@"Failed to remove notification");
	}
	sqlite3_finalize(statement);
	return ret;
}


+(NotificationDbHelper *)getInstance
{
	static NotificationDbHelper *instance;
	
	@synchronized(self) {
		if(instance==nil)
			instance = [[NotificationDbHelper alloc] init];
	}
	return instance;
}

@end


@implementation NotificationStack;

-(void)pushNotification:(NSString*)json {
	NotificationDbHelper* dbHelper = [NotificationDbHelper getInstance];
	if ( [dbHelper open] == NO ) {
		return;
	}
	[dbHelper insert:json];
	[dbHelper close];
}

-(NSMutableArray*) pullNotifications {
	NSMutableArray *notifs = NULL;

	NotificationDbHelper* dbHelper = [NotificationDbHelper getInstance];
	if ( [dbHelper open] == NO ) {
		return NULL;
	}
	NSMutableArray *ids = NULL;
	notifs = [dbHelper getAllAndIds:&ids];
	[dbHelper remove:ids];
	[dbHelper close];
	return notifs;
}

@end


@implementation NotificationManagerObject;
@synthesize notificationManagerPlugin;

- (id)init {
	if (self = [super init]) {
		ns = [NotificationStack alloc];
		lastAlertID = [[NSMutableDictionary alloc] init];
	}
	return self;
}

+ (NotificationManagerObject*) getInstance {
	static NotificationManagerObject *instance = nil;
	@synchronized(self) {
		if (instance == nil)
			instance = [[self alloc] init];
	}
	return instance;
}

-(void)onPushNotification:(NSDictionary *)notification {
	NSData* json = [NSJSONSerialization dataWithJSONObject:notification
												   options:NULL
													 error:NULL];
	NSString* jsonString = [[NSString alloc] initWithData:json
												 encoding:NSUTF8StringEncoding];

	[ns pushNotification:jsonString];

	@try {
		[self.notificationManagerPlugin onNotification:notification isPush:YES];
	} @catch (NSException *exception) {} @finally {}
}

-(void)onGeoNotification:(NSDictionary *)notification {
	
	// Check if notification should be processed. C.F.  zone/time threshold
	NSNumber* alertId = [notification objectForKey: @"id"];
	if ( alertId != nil ) {
		NSNumber* lastAlertTS = [lastAlertID objectForKey: [alertId stringValue] ];
		double ts = ( lastAlertTS != nil ) ? [lastAlertTS doubleValue] : 0;

		if ( (CFAbsoluteTimeGetCurrent() - ts) < AlertThresholdInSecond ) {
			NSLog(@"Geo notification is ignored (Threshold limitation)");
			return;
		}
	}
	// Add timestamp
	[ lastAlertID setValue:[NSNumber numberWithDouble:CFAbsoluteTimeGetCurrent()] forKey: [alertId stringValue] ];

	
	NSData* json = [NSJSONSerialization dataWithJSONObject:notification
												   options:NSJSONWritingPrettyPrinted
													 error:NULL];
	NSString* jsonString = [[NSString alloc] initWithData:json
												 encoding:NSUTF8StringEncoding];

	[ns pushNotification:jsonString];

	@try {
		[self.notificationManagerPlugin onNotification:notification isPush:NO];
	} @catch (NSException *exception) {} @finally {}
}

-(NSMutableArray*)getPendingNotifications {
	return [ns pullNotifications];
}

@end


@implementation NotificationManager;
@synthesize commandDelegate;

- (void)pluginInitialize
{
	NSLog(@"PushNotification: pluginInitialize!");
	NotificationManagerObject* nm = [NotificationManagerObject getInstance];
	nm.notificationManagerPlugin = self;
}

-(void)onNotification:(NSDictionary *)notification isPush:(BOOL)push {
	
	if ( UIApplication.sharedApplication.applicationState == UIApplicationStateActive )  {
		// App in foreground
		[[self commandDelegate] evalJs: @"var e = new Event('onNotification'); document.dispatchEvent(e);"];
	} else {

		NSString* title = nil;
		NSString* text = nil;

		// Push notifs are managed by iOS, so no need to duplicate it
		if ( !push ) {
			// this is a geo notification (from nao)
			NSString * lang1 = [[NSLocale preferredLanguages] firstObject];
            NSArray *langSplit = [lang1 componentsSeparatedByString:@"-"];
            NSString * lang = [langSplit objectAtIndex:0];


			if ( [notification objectForKey: @"title"] ) {
				
				NSDictionary* titleData = [notification objectForKey: @"title"];
				if ( titleData != nil ) {
					if ( [titleData objectForKey:lang] ) {
						title = [titleData objectForKey: lang];
					} else if ( [titleData objectForKey:@"en"] ) {
						title = [titleData objectForKey: @"en"];
					} else {
						NSLog( @"Cannot retreive the notification message langage" );
						return;
					}
				}
			} else if ( [notification objectForKey: @"name"] ) {
				title = [notification objectForKey: @"name"];
			}
			NSDictionary* msgData = nil;
			if ( [notification objectForKey: @"content"] ) {
				msgData = [notification objectForKey: @"content"];
			} else if ( [notification objectForKey: @"text"] ) {
				msgData = [notification objectForKey: @"text"];
			}

			if ( msgData != nil ) {
				if ( [msgData objectForKey:lang] ) {
					text = [msgData objectForKey: lang];
				} else if ( [msgData objectForKey:@"en"] ) {
					text = [msgData objectForKey: @"en"];
				} else {
					NSLog( @"Cannot retreive the notification message langage" );
					return;
				}
			}
		}

		if (@available(iOS 10, *)) {

			UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
			UNMutableNotificationContent *content = [UNMutableNotificationContent new];
			content.title = title;
			content.body = text;
			content.sound = [UNNotificationSound defaultSound];
			UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1 repeats:NO];
			UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"LocalNotification"
																				  content:content
																				  trigger:trigger];
			
			[center addNotificationRequest:request withCompletionHandler:^(NSError *_Nullable error) {
				if (error != nil) {
					NSLog(@"Something went wrong: %@", error);
				}
			}];
		} else {
			UILocalNotification *localNotification = [[UILocalNotification alloc] init];
			localNotification.fireDate = [NSDate date];
			localNotification.timeZone = [NSTimeZone defaultTimeZone];
			localNotification.alertBody = text;
			localNotification.timeZone = [NSTimeZone defaultTimeZone];
			[[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
		}
	}
}

- (void) getPendingNotifications:(CDVInvokedUrlCommand*)command {

	NotificationManagerObject* nm = [NotificationManagerObject getInstance];
	NSMutableArray* notifs = [nm getPendingNotifications];
	
	// Serialize to json
	NSDictionary* obj = [[NSMutableDictionary alloc] init];
	[obj setValue:notifs forKey:@"notifications"];
	NSData* json = [NSJSONSerialization dataWithJSONObject:obj
												   options:NSJSONWritingPrettyPrinted
													 error:NULL];
	NSString* jsonString = [[NSString alloc] initWithData:json
												 encoding:NSUTF8StringEncoding];
	
	// Pending notification callback
	CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: jsonString];
	[self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
