//
//  NotificationStack.h
//  Siae2019_D
//
//  Created by Aurélien Hiron on 15/04/2019.
//
#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>
#import <sqlite3.h>

@interface NotificationDbHelper : NSObject {
	sqlite3 *db;
}
-(BOOL)open;
-(void)close;
-(BOOL)insert:(NSString *)json;
-(NSMutableArray*)getAllAndIds:(NSMutableArray**)ids;
-(BOOL)remove:(NSArray*)ids;

@end

@interface NotificationStack : NSObject
-(void)pushNotification:(NSString*)json;
-(NSMutableArray*) pullNotifications;
@end

@interface NotificationManager : CDVPlugin
@property (nonatomic, weak) id <CDVCommandDelegate> commandDelegate;
-(void)getPendingNotifications:(CDVInvokedUrlCommand*)command;
-(void)onNotification:(NSDictionary *)notification isPush:(BOOL)push;
@end

@interface NotificationManagerObject : NSObject{
	NotificationStack* ns;
	NSString* lastZone;
	NSMutableDictionary* lastAlertID;
}
@property (nonatomic, weak) NotificationManager* notificationManagerPlugin;
+ (NotificationManagerObject*) getInstance;
-(void)onPushNotification:(NSDictionary *)notification;
-(void)onGeoNotification:(NSDictionary*) jsonString;
-(NSMutableArray*)getPendingNotifications;
@end

